"use strict";

/* 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
События клавиатуры считываются с окна вцелом, но можно и к инпуту. Пользователь может скопировать символы с другого места и вставить в инпут, но уже есть скрипты для защиты от таких символов */

const arrBtns = Array.from(document.querySelectorAll(".btn-wrapper .btn"));
let arrText = [];
arrBtns.forEach((el) => {
  arrText.push(el.textContent.toUpperCase());
});

document.addEventListener("keydown", function (e) {
  arrBtns.forEach((el) => {
    el.style.backgroundColor = "#000000";
  });

  for (let i = 0; i < arrText.length; i++) {
    if (e.key.toUpperCase() === arrText[i]) {
      arrBtns[i].style.backgroundColor = "blue";
      break;
    }
  }
});
